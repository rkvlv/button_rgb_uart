/*
 * use gdb, buttons and uart functions of stellaris ware library
 */
#include <inc/hw_types.h>
#include <inc/hw_memmap.h>
#include <driverlib/debug.h>
#include <driverlib/gpio.h>
#include <driverlib/rom.h>
#include <driverlib/sysctl.h>
#include <drivers/rgb.h>
#include <drivers/buttons.h>
#include <utils/uartstdio.h>
#include <utils/cpu_usage.h>
#include <driverlib/systick.h>
#include <driverlib/interrupt.h>

static void enableRGB(unsigned long ulColors[], float intensity) {
    RGBInit(0);
    //
    // Set the intensity level from 0.0f to 1.0f
    //
    RGBIntensitySet(intensity);
    //
    // Initialize the three color values.
    //
    ulColors[BLUE] = 0x0FFF;
    ulColors[RED]  = 0x0FFF;
    ulColors[GREEN] = 0x0FFF;
    RGBColorSet(ulColors);
    //
    // Enable the RGB. This configure GPIOs to the Timer PWM mode needed
    // to generate the color spectrum.
    //
    RGBEnable();
    SysCtlPeripheralSleepEnable(RED_GPIO_PERIPH);
    SysCtlPeripheralSleepEnable(RED_TIMER_PERIPH);

    SysCtlPeripheralSleepEnable(GREEN_GPIO_PERIPH);
    SysCtlPeripheralSleepEnable(GREEN_TIMER_PERIPH);

    SysCtlPeripheralSleepEnable(BLUE_GPIO_PERIPH);
    SysCtlPeripheralSleepEnable(BLUE_TIMER_PERIPH);

}

static void decIntensity(float *intensity)
{
    *intensity -= 0.05f;
    if(*intensity <= 0.0f) {
        *intensity = 0.05f;
    }
    RGBIntensitySet(*intensity);
}

static void incIntensity(float *intensity)
{
    *intensity += 0.05f;
    if(*intensity >= 1.0f) {
        *intensity = 0.95f;
    }
    RGBIntensitySet(*intensity);
}

static void enableButtons()
{
    ButtonsInit();
}

static void enableUART()
{
    //
    // Configure the appropriate pins as UART pins; in this case, PA0/PA1 are
    // used for UART0.
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    //
    // Initialize the UART standard IO module.
    //
    UARTStdioInit(0);
    SysCtlPeripheralSleepEnable(SYSCTL_PERIPH_UART0);
}

void SysTickIntHandler(void)
{
    static unsigned counter = 0;
    //
    // Compute the CPU usage for the last time period.
    //
    unsigned long CpuUsage = CPUUsageTick();
    if(++ counter % 300 == 0) {
        UARTprintf("\rCpu usage %3u.%u%%%20s\r", CpuUsage >> 16, CpuUsage << 16, "");
    }
}

static void enableCpuUsage()
{
    //
    // Initialize the CPU usage module, using timer 0.
    //
    CPUUsageInit(SysCtlClockGet(), 1000, 2);
    SysTickPeriodSet(SysCtlClockGet() / 1000);
    SysTickIntRegister(SysTickIntHandler);
    SysTickIntEnable();
    SysTickEnable();
}

int main() {
    /* Set clock to 80MHz */
    ROM_SysCtlClockSet(
            SYSCTL_SYSDIV_2_5
           |SYSCTL_USE_PLL
           |SYSCTL_XTAL_16MHZ
           |SYSCTL_OSC_MAIN
    );
    unsigned long ulColors[3];
    unsigned char ucDelata, ucState;
    float rgbIntensity = 0.5f;

    enableRGB(ulColors, rgbIntensity);
    enableButtons();
    enableUART();
    enableCpuUsage();

    for(;;) {
        ucState = ButtonsPoll(&ucDelata, 0);
        if(BUTTON_RELEASED(LEFT_BUTTON, ucState, ucDelata)) {
            decIntensity(&rgbIntensity);
            ROM_SysCtlDelay(SysCtlClockGet() / 2 / 3);
        } else if(BUTTON_RELEASED(RIGHT_BUTTON, ucState, ucDelata)) {
            incIntensity(&rgbIntensity);
            ROM_SysCtlDelay(SysCtlClockGet() / 2 / 3);
        }
        ROM_SysCtlSleep();
    }
}
